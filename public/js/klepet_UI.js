/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo, uporabnisko_ime, nadimek) {
  
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    
    if(nadimek != null){
      let besede = sporocilo.split(' ');
      let ime = besede[0].split(':');
      let dvopicje = besede[0].charAt(besede[0].length - 1) == ':' ? true : false;
      
      besede[0] = nadimek + " (" + ime[0] + ")" + (dvopicje ? ":" : "");
      
      sporocilo = besede.join(' ');
    }
    return $('<div style="font-weight: bold" onclick="krcni(\''+uporabnisko_ime+'\')"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div class="sistemsko_sporocilo"></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      
      prikaziSliko(sistemskoSporocilo);
      
      let besede = sporocilo.split(' ');
      let ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
      besede.shift();
      let besedilo = besede.join(' ');
      let parametri = besedilo.split('\"');
      let uporabniskoIme = parametri[1];
      
      nadimki.forEach(function(value, key, map){
          if(parametri[1]==value)
            uporabniskoIme = key;
        });
      
      if(ukaz == "zasebno"){
        let objekt = $(".sistemsko_sporocilo").last();
        objekt.attr("onclick", "krcni('"+uporabniskoIme+"')");
      }
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);

    $('#sporocila').append(divElementEnostavniTekst(sporocilo, "dont_click", null));


    
    //preveri če je slika in jo doda če je
    prikaziSliko(sporocilo);

    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
var klepetApp;
$(document).ready(function() {
  klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    
    var novElement = divElementEnostavniTekst(sporocilo.besedilo, sporocilo.uporabnik, nadimki.get(sporocilo.uporabnik));
    $('#sporocila').append(novElement);
    
    prikaziSliko(sporocilo.besedilo);
    
    var regex = / se je preimenoval v /;
    if(regex.test(sporocilo.besedilo)){
      var besede = sporocilo.besedilo.split(' ');
      var staroIme = besede[0];
      var novoIme = besede[besede.length - 1].substring(0, besede[besede.length - 1].length - 1);
      console.log(staroIme +" -> "+novoIme);
      
      let nadimek = nadimki.get(staroIme);
      nadimki.delete(staroIme);
      nadimki.set(novoIme, nadimek);
    }
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i], "dont_click", null));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    var tempNadimki = new Map();
    
    for (var i=0; i < uporabniki.length; i++) {

      tempNadimki.set(uporabniki[i], nadimki.get(uporabniki[i]));
      
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i], uporabniki[i], nadimki.get(uporabniki[i])));
    }
    nadimki = tempNadimki;
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});


/*
MOJE FUNKCIJE
*/

function krcni(uporabnisko_ime){
  if(uporabnisko_ime != "dont_click"){
    let sporocilo = klepetApp.procesirajUkaz('/zasebno "'+uporabnisko_ime+'" "'+String.fromCharCode(9756)+'"');
     $('#sporocila').append(divElementEnostavniTekst(sporocilo, uporabnisko_ime));
  }
}

function prikaziSliko(besedilo){
  
  let besede = besedilo.split(' ');
  let url = [];
  
  for(var i=0; i<besede.length; i++){
    var pattern =  /(http\S+\.(?:jpg|png|gif))/g;
    if(pattern.test(besede[i])){
      url[url.length] = besede[i];
    }
  }
  
  for(var i=0; i<url.length; i++){
    var image = '<br/><img style="width: 200px; padding-left: 20px;" src="'+url[i]+'">';
    $('#sporocila').append(image);
    
  }
    
}

var nadimki = new Map();
